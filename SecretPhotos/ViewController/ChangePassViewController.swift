import UIKit

class ChangePassViewController: UIViewController {

    @IBOutlet var passTextField: UITextField!
    @IBOutlet var reenterPassTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func changePassButton(_ sender: UIButton) {
        if((passTextField.text != reenterPassTextField.text) && passTextField.text != "") {
            sender.isEnabled = false
        } else {
            UserDefaults.standard.set(passTextField.text, forKey: "password")
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func goBackButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
       }
       
}
