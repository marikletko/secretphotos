import UIKit
import CoreData
private let reuseIdentifier = "Cell"

class ImagesCollectionViewController: UICollectionViewController {
    
    var photos = [Photos]()
    
    let imagePicker = UIImagePickerController()
    var imageProperties:[ImageProperties] = UserDefaults.standard.value([ImageProperties].self, forKey: "ImageProperties") ?? []
    
    @IBAction func changePasswordButton(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "ChangePassViewController") as? ChangePassViewController else { return }
                      self.navigationController?.pushViewController(controller, animated: true)
    }
        
    
    
    @IBAction func addPhotoButton(_ sender: UIButton) {
        pick()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let fetchRequest: NSFetchRequest<Photos> = Photos.fetchRequest()
        do {
            let image = try PersistenceServce.context.fetch(fetchRequest)
            self.photos = image
            self.collectionView.reloadData()
        } catch {}
        self.collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoItemCell
        let dataImage = photos[indexPath.row].photos
        let image = UIImage(data: dataImage!)
        cell.img.image = image
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc=ImagePreviewVC()
        vc.imageProperties = self.imageProperties
        vc.passedContentOffset = indexPath
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.size.width/3
        let height = width
        return CGSize(width: width, height: height)
    }
    
    func pick() {
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
}

