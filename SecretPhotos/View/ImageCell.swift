import Foundation
import UIKit


class ImagePreviewFullViewCell: UICollectionViewCell {
var scrollImage:ImageZoomView!
var lovesView:UIButton!
var deleteImageView:UIButton!
var signView:UIButton!
var signViewTextField:CustomTextField!
var bottomView:UIView!
 
    
override init(frame: CGRect) {
    super.init(frame: frame)
    scrollImage = ImageZoomView(frame: CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight))
    scrollImage.setup()
    scrollImage.imageContentMode = .aspectFit
    scrollImage.initialOffset = .center
    bottomView = UIView(frame: CGRect(x: 0, y: viewHeight - 50, width: viewWidth, height: 50))
    bottomView.backgroundColor = .white
    bottomView.isHidden = false
    
    lovesView = UIButton(frame: CGRect(x: viewWidth/2 - 20, y: 0, width: 40, height: 40))
    lovesView.backgroundColor = .white
    
    deleteImageView = UIButton(frame: CGRect(x: 10, y: 0, width: 40, height: 40))
    deleteImageView.backgroundColor = .white
    
    signView = UIButton(frame: CGRect(x: 300, y: 0, width: 40, height: 40)) 
    signView.backgroundColor = .white
    
    signViewTextField = CustomTextField(frame: CGRect(x: 50, y: 200, width: 200, height: 50))
    signViewTextField.backgroundColor = .white
    signViewTextField.frameToInters = bottomView

    
    bottomView.addSubview(lovesView)
    bottomView.addSubview(deleteImageView)
    bottomView.addSubview(signView)
    
    self.addSubview(bottomView)
    self.addSubview(scrollImage)
    self.bringSubviewToFront(bottomView)
}

required init?(coder aDecoder: NSCoder) {
     fatalError("init(coder:) has not been implemented")
 }

}

class PhotoItemCell: UICollectionViewCell {
    
@IBOutlet var img: UIImageView!
    
}


protocol CustomTextFieldDelegate: AnyObject {
    func deleteTextField()
    func saveTextFieldPos(positionText: CGPoint)
   }

class CustomTextField: UITextField {
    var lastLocation = CGPoint(x:0,y:0)
    var frameToInters: UIView?
   
    weak var delegates: CustomTextFieldDelegate?
    override init(frame:CGRect) {
        super.init(frame: frame)
    
        let panRecognizer = UIPanGestureRecognizer(target: self, action:#selector(CustomTextField.findPan(_:)))
        self.gestureRecognizers = [panRecognizer]
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func findPan(_ panRecognizer:UIPanGestureRecognizer) {
        let translationOfView = panRecognizer.location(in: self.superview)
        self.center = translationOfView
        if(self.frame.intersects(frameToInters!.frame)) {
                                 self.removeFromSuperview()
            delegates?.deleteTextField()
                               }
        
        if panRecognizer.state == .ended {
            let panRecognizerPos = panRecognizer.location(in: superview)
            let positionText = CGPoint(x: panRecognizerPos.x - self.frame.size.width/2, y: panRecognizerPos.y - self.frame.size.height/2)
            delegates?.saveTextFieldPos(positionText: positionText)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.superview?.bringSubviewToFront(self)
              lastLocation = self.center
    }
}


