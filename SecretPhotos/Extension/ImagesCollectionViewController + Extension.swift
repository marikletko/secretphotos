import Foundation
import UIKit

extension ImagesCollectionViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegateFlowLayout {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            
            let dataImage = pickedImage.jpegData(compressionQuality: 1)
            
            let image = Photos(context: PersistenceServce.context)
            image.photos = dataImage
            PersistenceServce.saveContext()
            photos.append(image)
             
            let obj = ImageProperties()
     
            obj.loves = false
            obj.text = nil
            obj.textPos = CGPoint(x:viewWidth/2,y:viewHeight/2)
            
            imageProperties.append(obj)
            UserDefaults.standard.set(encodable: self.imageProperties, forKey: "ImageProperties")

            self.imagePicker.dismiss(animated: true) {
            self.collectionView.reloadData()
            }
        }
    }
    
}
